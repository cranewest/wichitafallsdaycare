<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-12 large-12 columns p_content">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

				<?php //comments_template( '', true ); ?>

			<?php endwhile; // end of the loop. ?>
		</div>

		<?php //get_sidebar(); ?>
	</div>

<?php get_footer(); ?>