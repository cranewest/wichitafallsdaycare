<?php
/**
 * Staff Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_locations_init() {
	// single uppercase
	$su = 'Location';
	// plural uppercase
	$pu = 'Locations';
	// single lowercase
	$sl = 'location';
	// plural lowercase
	$pl = 'locations';

	$field_args = array(
		'labels' => array(
			'name' => __( $pu ),
			'singular_name' => __( $pu ),
			'add_new' => __( 'Add New '.$su ),
			'add_new_item' => __( 'Add New '.$su ),
			'edit_item' => __( 'Edit '.$su ),
			'new_item' => __( 'Add New '.$su ),
			'view_item' => __( 'View '.$su ),
			'search_items' => __( 'Search '.$pu ),
			'not_found' => __( 'No '.$pl.' found' ),
			'not_found_in_trash' => __( 'No '.$pl.' found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => true,
		'has_archive' => true,
		'menu_position' => 18,
		'supports' => array('title', 'thumbnail'),
		'menu_icon' => 'dashicons-location-alt'
	);
	register_post_type('locations',$field_args);
}
add_action( 'init', 'cw_cpp_locations_init' );


/**
 * Add Categories to Custom Post Type
 *
 * This is just an example of adding categories to a custom post type.
 * To see in action just uncomment and it will add categories to locations.
 *
 * @since CW 1.0
 */
// function cw_cpp_locations_categories() {
// 	$field_args = array(
// 		'labels' => array(
// 			'name'              => _x( 'Categories', 'taxonomy general name' ),
// 			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
// 			'search_items'      => __( 'Search Categories' ),
// 			'all_items'         => __( 'All Categories' ),
// 			'parent_item'       => __( 'Parent Category' ),
// 			'parent_item_colon' => __( 'Parent Category:' ),
// 			'edit_item'         => __( 'Edit Category' ),
// 			'update_item'       => __( 'Update Category' ),
// 			'add_new_item'      => __( 'Add New Category' ),
// 			'new_item_name'     => __( 'New Category' ),
// 			'menu_name'         => __( 'Categories' ),
// 		),
// 		'hierarchical' => true
// 	);
// 	register_taxonomy( 'locations_categories', 'locations', $field_args );
// }
// add_action( 'init', 'cw_cpp_locations_categories', 0 );