<?php
/**
 * Slides Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_slides_init() {
	$field_args = array(
		'labels' => array(
			'name' => __( 'Slides' ),
			'singular_name' => __( 'Slides' ),
			'add_new' => __( 'Add New Slide' ),
			'add_new_item' => __( 'Add New Slide' ),
			'edit_item' => __( 'Edit Slide' ),
			'new_item' => __( 'Add New Slide' ),
			'view_item' => __( 'View Slide' ),
			'search_items' => __( 'Search Slides' ),
			'not_found' => __( 'No slides found' ),
			'not_found_in_trash' => __( 'No slides found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => true,
		'menu_position' => 20,
		'supports' => array('title'),
        'menu_icon' => 'dashicons-images-alt'
	);
	register_post_type('slides',$field_args);
}
add_action( 'init', 'cw_cpp_slides_init' );



// /**
//  * Add Categories to Custom Post Type
//  *
//  * This is just an example of adding categories to a custom post type.
//  * To see in action just uncomment and it will add categories to slides.
//  *
//  * @since CW 1.0
//  */
// // function cw_cpp_slides_categories() {
// // 	$field_args = array(
// // 		'labels' => array(
// // 			'name'              => _x( 'Categories', 'taxonomy general name' ),
// // 			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
// // 			'search_items'      => __( 'Search Categories' ),
// // 			'all_items'         => __( 'All Categories' ),
// // 			'parent_item'       => __( 'Parent Category' ),
// // 			'parent_item_colon' => __( 'Parent Category:' ),
// // 			'edit_item'         => __( 'Edit Category' ),
// // 			'update_item'       => __( 'Update Category' ),
// // 			'add_new_item'      => __( 'Add New Category' ),
// // 			'new_item_name'     => __( 'New Category' ),
// // 			'menu_name'         => __( 'Categories' ),
// // 		),
// // 		'hierarchical' => true
// // 	);
// // 	register_taxonomy( 'slides_categories', 'slides', $field_args );
// // }
// // add_action( 'init', 'cw_cpp_slides_categories', 0 );