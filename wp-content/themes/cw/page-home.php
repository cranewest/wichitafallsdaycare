<?php
/* Template Name: Home Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<?php get_template_part('content', 'slides'); ?>
		<div class="small-12 medium-12 large-12 columns div_cont">

			<?php
				// if(have_posts()) {
				// 	while(have_posts()) {
				// 		the_post();
				// 		//echo '<h2 class="page-title">'.get_the_title().'</h2>';
				// 		the_content();
				// 		// comments_template( '', true );
				// 	}
				// }
			?>
			<div class="small-12 medium-4 large-4 columns ">
				<div class="small-12 medium-12 large-12 columns location">
					<h3><strong class="h_title">LOCATION</strong></h3>
					<h5 class="w_txt"><strong>Where Are We</strong></h5>
					<h6 class="t_height">View our location & get directions</h6><br><br>

					<div class=" img_padding center"><img src="<?php bloginfo('template_directory'); ?>/img/pictures/wig-gig_mainPage_img1.png"></div>
					<hr class="hr_inv">
					<a href="/location/" class="button button1"><i>Directions</i></a>
				</div>

			</div>

			<div class="small-12 medium-4 large-4 columns ">
				<div class="small-12 medium-12 large-12 columns program">
					<h3><strong class="h_title">PROGRAM</strong></h3>
					<h5 class="w_txt"><strong>What Our Kids Do</strong></h5>
					<h6 class="t_height1">Take a look at what we offer during the day</h6><br>

					<div class="img_padding center"><img src="<?php bloginfo('template_directory'); ?>/img/pictures/wig-gig_mainPage_img2.png"></div>
					<hr class="hr_inv">
					<a href="/program/" class="button button2"><i>More</i></a>
				</div>
			</div>

			<div class="small-12 medium-4 large-4 columns ">
				<div class="small-12 medium-12 large-12 columns ourday">
					<h3><strong class="h_title">OUR DAY</strong></h3>
					<h5 class="w_txt"><strong>Activities & Schedules</strong></h5>
					<h6 class="t_height">Take a look at our Daily activities</h6><br><br>

					<div class=" img_padding center"><img src="<?php bloginfo('template_directory'); ?>/img/pictures/wig-gig_mainPage_img3.png"></div>
					<hr class="hr_inv">
					<a href="/our-day" class="button button3"><i>More</i></a>

				</div>
			</div>

		</div>

		<?php //get_sidebar(); ?>
	</div>

<?php get_footer(); ?>