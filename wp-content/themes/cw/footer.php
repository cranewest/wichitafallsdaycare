<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
<div class="footer_bg">
	<footer role="contentinfo" class="row ">

		<div class="small-12 medium-4 large-4 columns footer_divs">

					<?$title = str_replace("\'", "'",cw_options_get_option( 'cwo_name' ));?>
					<?echo $title;?><br>
					<?echo cw_options_get_option( 'cwo_address1' );?><br>
					<?echo cw_options_get_option( 'cwo_city' );?>,
					<?echo cw_options_get_option( 'cwo_state' );?>
					<?echo cw_options_get_option( 'cwo_zip' );?><br>
					<?echo '<a href="tel:'.preg_replace("/[^0-9]/","",cw_options_get_option( 'cwo_phone' )).'">'.cw_options_get_option( 'cwo_phone' ).'</a>';?><br>
					<?echo '<a href= "mailto:'.cw_options_get_option( 'cwo_email' ).'" >'.cw_options_get_option( 'cwo_email' ).'</a>';?>

		</div>
		<div class="small-12 medium-4 large-4 columns footer_divs">

				Hours:<br>
				Monday - Friday <br>6:00am to 5:30pm <br>

		</div>
		<div class="small-12 medium-4 large-4 columns footer_divs1">
			<a target="_blank" href="https://www.facebook.com/wigglesngiggleschildcare?_rdr"><img class="footer_social" src="<?php bloginfo('template_directory'); ?>/img/socialMedia/wig-gig_socialMedia_fb.png"></a>
			<a target="_blank" href="https://plus.google.com/103586488808952506903/about"><img class="footer_social" src="<?php bloginfo('template_directory'); ?>/img/socialMedia/wig-gig_socialMedia_g.png"></a>
		</div>

		<div class="large-12 columns c_txt">
			<p>&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>, All Rights Reserved.</p>
			<!-- <a class="cw" href="http://crane-west.com/">Site Powered by Crane | West</a> -->
		</div>
		
	</footer>
</div>

	<!-- WP_FOOTER() -->
	<?php wp_footer(); ?>
	<?php
		// must activate CW Options Page plugin for the line below to work
		$ga_code = cw_options_get_option('cwo_ga'); if( !empty($ga_code) ) {
	?>
	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo $ga_code; ?>', 'auto');
		ga('send', 'pageview');
	</script>
	<?php } ?>
</body>
</html>