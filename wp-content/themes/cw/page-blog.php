<?php
/* Template Name: Blog Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<?php get_template_part('content', 'slides'); ?>
		<div class="small-12 medium-12 large-12 columns p_content">

			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						echo '<h2 class="page-title">'.get_the_title().'</h2>';
						//the_content();
						// comments_template( '', true );
					}
				}
			?>

			<?php 	
				//adjusting the query
				$args = array(
					// 'category_name' => 'news',
					'post_type' => 'post',
					'posts_per_page' => -1,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						echo'<div class = "">';
							$n_link = get_post_meta($post->ID, '_cwmb_link', true);
							
							?><a href="<? the_permalink(); ?>"><h5 class="h_grey"><? echo get_the_title(); ?></h5></a><?

							//echo'<div class="grey_bg_title columns small-12 medium-3 large-3"><h6 class="news_date">Testimonials</h6></div>';
							//echo'<div class="columns small-12 medium-3 large-3"><h6 class="news_date">'. get_the_date(). '</h6><br></div>';
			
							echo'<div class="act_content">';
								the_excerpt();
							echo'</div>';

							?><a class=""href="<?php the_permalink(); ?>">Read More</a><?

						echo'</div><hr>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
		</div>

		<?php //get_sidebar(); ?>
	</div>

<?php get_footer(); ?>